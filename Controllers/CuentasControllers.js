const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuaeb10ed/collections/";
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;



function getCuentasById(req, res) {
  console.log("GET /apitechu/v2/cuentas/:idUsuario");
//creamos variable donde nos llega la variable del id
  var id =req.params.idUsuario;
  var query = 'q={"idUsuario":' + id + '}';

  console.log("la consulta es: " + query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  httpClient.get("cuentas?" + query + "&" + mLabAPIKEY,
    function( err, resMlab, body) {
      if (err){
        var response = {
          "msg" :"Error obteniendo las cuentas del usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
              "msg" :"Usuario de la cuenta no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

module.exports.getCuentasById = getCuentasById;
