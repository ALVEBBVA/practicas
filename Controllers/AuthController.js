const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuaeb10ed/collections/";
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;


function userlogin (req, res) {
  console.log("POST /apitechu/v1/login");

//traemos fichero de usuarios
var users = require('../usuarios.json');
var result = {};
userfound = false
var i;
for (i=0; i<users.length; i++)
{
  console.log("valor users-email  :" + users[i].email);
  console.log("valor body-email   :" +  req.body.email);
  console.log("valor users-Pass   :" + users[i].Pass);
  console.log("valor body-password:" +  req.body.password);
   if ( users[i].email == req.body.email ){
   //&& users[i].Pass == req.body.password
     console.log("SI entro en el if valor mail" + i);
    userfound = true;
     users[i].logged = true;
     result.id = users[i].id;
     io.writeUserDatatoFile(users);
     res.send({"mensaje" : "login correcto :) ", "idUsuario" : result.id});
//     break;

   }
}
   if ( userfound == false) {

   console.log("NO ENCUENTRO MAIL Y PASSWORD");
   res.send({"msg" : "login incorrecto"});
//    break;
   }
 }
//----------------
function userloginV2 (req, res) {
  console.log("POST /apitechu/v2/login");
  var query = 'q={"email": "' + req.body.email + '"}';

  console.log("query es " + query);

  httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("user?" + query + "&" + mLabAPIKEY,

  function(err, resMLab, body) {
      var isPasswordcorrect =
      crypt.checkPassword(req.body.password, body[0].password);
    console.log("Password correct is " + isPasswordcorrect);
     if (!isPasswordcorrect) {
      var response = {
        "mensaje" : "Login incorrecto, el password no se ha encontrado"
      }
      res.status(401);
      res.send(response);
    } else {
      console.log("encontrado user con email y password, logging in");
      query = 'q={"id" : ' + body[0].id +'}';
      console.log("Query for put is " + query);
      var putBody = '{"$set":{"logged":true}}';
      httpClient.put("user?" + query + "&" + mLabAPIKEY, JSON.parse(putBody),
        function(errPUT, resMLabPUT, bodyPUT) {
          console.log("PUT done");
          var response = {
            "msg" : "Usuario logado con éxito",
            "idUsuario" : body[0].id
          }
          res.send(response);
        }
      )
    }
  }
);
}

function userlogout (req, res) {
  console.log("POST /apitechu/v1/logout/:id");

//traemos fichero de usuarios a continuacion
var users = require('../usuarios.json');
var result = {};
userfound = false
var i;
for (i=0; i<users.length; i++)
{
  console.log("valor req.params.id  :" + req.params.id);
  console.log("valor users[i].id   :" +  users[i].id);

  if (users[i].id == req.params.id && users[i].logged == true){
   console.log(" usuario logado = id parametro de entrada");

   userfound = true;
   delete users[i].logged;
   result.id = users[i].id;
   io.writeUserDatatoFile(users);
   res.send({"mensaje" : "logout correcto :) ", "idUsuario" : result.id});
   break;
 }
}
   if ( userfound == false) {

   console.log("NO ENCUENTRO usuario a realizar logout");
   res.send({"msg" : "NO ENCUENTRO usuario a realizar logout"});
//    break;
   }
 }
//---------------------------
function userlogoutV2(req, res) {

 console.log("POST /apitechu/v2/logout/:id");
 var query = 'q={"id": ' + req.params.id + '}';

 console.log("query es " + query);
 httpClient = requestJson.createClient(baseMlabURL);

 httpClient.get("user?" + query + "&" + mLabAPIKEY,
   function( err, resMlab, body) {

     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
//operadores de mongodb (unset)
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKEY, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}
//

module.exports.userlogin = userlogin ;
module.exports.userloginV2 = userloginV2 ;
module.exports.userlogout = userlogout ;
module.exports.userlogoutV2 = userlogoutV2;
