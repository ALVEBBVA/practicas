const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuaeb10ed/collections/";
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1(req, res) {
 console.log("GET /apitechu/v1/users");
//   res.sendFile('usuarios.json',{root:__dirname});

var result = {};
var users = require('../usuarios.json');
if (req.query.$count == "true") {
console.log("Count needed");
result.count = users.length;
}

result.users = req.query.$top ?
users.slice(0, req.query.$top) : users;


 res.send(result);
}

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKEY,
    function( err, resMlab, body) {
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send (response);
    }
  )
}

function getUserByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");
//creamos variable donde nos llega la variable del id
  var id =req.params.id;
  var query = 'q={"id":' + id + '}';

  console.log("la consulta es: " + query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKEY,
    function( err, resMlab, body) {
      if (err){
        var response = {
          "msg" :"Error obteniendo el Usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];

        } else {
          var response = {
              "msg" :"Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}




function createUserV1 (req, res) {
  console.log("POST /apitechu/v1/users");

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email
  }
//traemos fichero de usuario

  var users = require('../usuarios.json');
  users.push(newUser);
//invocamos a la funcion nueva
  io.writeUserDatatoFile(users);
  console.log("Usuario añadido con exito");
  res.send({"msg" : "Usuario añadido con exito"})

  }

  function createUserV2 (req, res) {
    console.log("POST /apitechu/v2/users");

    console.log(req.body.id);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
    console.log(req.body.Pass);

    var newUser = {
      "id": req.body.id,
      "first_name": req.body.first_name,
      "last_name":req.body.last_name,
      "email":req.body.email,
      "password": crypt.hash(req.body.Pass)
    }

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  httpClient.post("user?" + mLabAPIKEY, newUser,
    function( err, resMlab, body) {
      console.log("Usuario creado en Mlab");
      res.status(201).send ({"msg": "Usuario creado"});
    }
  )
}


    function deleteUserV1(req, res){
      console.log("DELETE /apitechu/v1/users/:id");
      console.log("Id es " + req.params.id);
  //traemos el fichero de usuarios
      var users= require('../usuarios.json');
  //---------
  //Vamos a realizar un bucle por lo que ya asteriscamos el splice
  //    users.splice(req.params.id -1, 1);
  //----------------------------
  // Filtrar con For (representa el indice)
      var i;
      for (i=0; i<users.length; i++)
      {
         if ( users[i].id == req.params.id)
         {
           console.log("entro en el if valor users" + i);
           users.splice(i ,1);
          console.log("Aplicado splice");
           io.writeUserDatatoFile(users);
           res.send({"msg" : "Usuario borrador con exito"})
           break;
         }
       }
    }


module.exports.getUsersV1 = getUsersV1 ;
module.exports.getUsersV2 = getUsersV2 ;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV1 = createUserV1 ;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1 ;
